package edu.ib.projekt_bazy_danych_v2;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Objects;

public class StatystykiAdminController {
    private Grapher grapher;
    private ObservableList<Object> list = FXCollections.observableArrayList();
    private ObservableList<Object> list2 = FXCollections.observableArrayList();

    @FXML
    private ComboBox cbOkres;

    @FXML
    private ComboBox cbSzczepionka;

    @FXML
    private BarChart<?, ?> graph;

    @FXML
    private DatePicker dpOd;

    @FXML
    private DatePicker dpDo;

    @FXML
    void initialize() {
        cbOkres.setItems(list);
        ArrayList listOkres = new ArrayList<>();
        listOkres.add("Rok");
        listOkres.add("Miesiąc");
        list.addAll(listOkres);

        cbSzczepionka.setItems(list2);
        ArrayList listSzczepionki = new ArrayList<>();
        Szczepionki szczepionki = new Szczepionki();
        szczepionki.getHashmap();
        listSzczepionki = szczepionki.getKeys();
        list2.addAll(listSzczepionki);

        grapher = new Grapher(graph);
    }

    public void btnWykresClicked(ActionEvent actionEvent) throws ClassNotFoundException, SQLException {
        if (cbOkres.getValue() == null || Objects.equals(dpOd.getValue(), null) || Objects.equals(dpDo.getValue(), null))
            JOptionPane.showMessageDialog(null, "Wprowadz daty oraz okres, aby zobaczyc statystyki.");
        else {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection;
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/punkt_szczepien?useUnicode=true&characterEncoding=utf-8&serverTimezone=CET");

            String szczepionka = String.valueOf(cbSzczepionka.getValue());
            if (cbSzczepionka.getValue() == null)
                szczepionka = "";

            String dataOd = String.valueOf(dpOd.getValue());
            String dataDo = String.valueOf(dpDo.getValue());

            ArrayList<String> xList = new ArrayList<>();
            ArrayList<Integer> yList = new ArrayList<>();

            if (cbOkres.getValue() == "Rok") {
                int dataOdInt = Integer.parseInt(dataOd.substring(0, 4));
                int dataDoInt = Integer.parseInt(dataDo.substring(0, 4));
                for (int i = dataOdInt; i <= dataDoInt; i++) {
                    xList.add(String.valueOf(i));
                    String sql = "SELECT COUNT(nazwa_szczepienia) FROM archiwum WHERE data like'%" + i + "%' AND data >= '" + dataOd + "' AND data <= '" + dataDo + "' AND nazwa_szczepienia LIKE'%" + szczepionka + "%';";
                    Statement st = connection.createStatement();
                    ResultSet rs = st.executeQuery(sql);
                    rs.next();
                    yList.add(rs.getInt(1));
                }
            } else if (cbOkres.getValue() == "Miesiąc") {
                long monthsBeetween = ChronoUnit.MONTHS.between(LocalDate.parse(String.valueOf(dpOd.getValue())), LocalDate.parse(String.valueOf(dpDo.getValue())));
                int rokOd = Integer.parseInt(dataOd.substring(0, 4));
                int miesiacOd = Integer.parseInt(dataOd.substring(5, 7));
                int rok = rokOd;
                int miesiac = miesiacOd;
                for (int i = 0; i <= (int) monthsBeetween; i++) {
                    if (miesiac < 10)
                        xList.add(rok + "-0" + miesiac);
                    else
                        xList.add(rok + "-" + miesiac);
                    String sql;
                    if (miesiac < 10) {
                        sql = "SELECT COUNT(nazwa_szczepienia) FROM archiwum WHERE data like'%" + rok + "-0" + miesiac + "%' AND data >='" + dataOd + "' AND data <= '" + dataDo + "' AND nazwa_szczepienia LIKE'%" + szczepionka + "%';";
                    } else {
                        sql = "SELECT COUNT(nazwa_szczepienia) FROM archiwum WHERE data like'%" + rok + "-" + miesiac + "%' AND data >= '" + dataOd + "' AND data <= '" + dataDo + "' AND nazwa_szczepienia LIKE'%" + szczepionka + "%';";
                    }
                    Statement st = connection.createStatement();
                    System.out.println(sql);
                    ResultSet rs = st.executeQuery(sql);
                    rs.next();
                    System.out.println(rs);
                    if (miesiac == 12) {
                        miesiac = 1;
                        rok += 1;
                    } else
                        miesiac += 1;
                    yList.add(rs.getInt(1));
                }
            }
            grapher.makePlot(xList, yList);
        }
    }

    public void btnPowrotClicked(ActionEvent event) throws IOException {
        ((Stage) (((Button) event.getSource()).getScene().getWindow())).close();
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("panel_admin.fxml")));
        Scene scene = new Scene(root, 700, 500);
        Stage primaryStage = new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void btnWyczyscClicked(ActionEvent event) {
        cbSzczepionka.getSelectionModel().clearSelection();
        cbSzczepionka.setItems(list2);
        grapher.clear();
    }
}