package edu.ib.projekt_bazy_danych_v2;

import javafx.scene.chart.XYChart;

import java.util.ArrayList;

public class Grapher {
    private XYChart graph;

    public Grapher(XYChart graph) {
        this.graph = graph;
    }

    public XYChart getGraph() {
        return graph;
    }

    public void setGraph(XYChart graph) {
        this.graph = graph;
    }

    public void makePlot(ArrayList<String> xList, ArrayList<Integer> tList) {
        XYChart.Series series = new XYChart.Series<>();
        for (int i = 0; i < xList.size(); i++)
            plotPoint(xList.get(i), tList.get(i), series);
        graph.getData().add(series);
    }

    private void plotPoint(String x, int y, XYChart.Series series) {
        series.getData().add(new XYChart.Data(x, y));
    }

    public void clear() {
        graph.getData().clear();
    }
}