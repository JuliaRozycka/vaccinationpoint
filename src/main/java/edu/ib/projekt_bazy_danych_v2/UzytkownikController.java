package edu.ib.projekt_bazy_danych_v2;

import entity.WidokUzytkownika;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Objects;

public class UzytkownikController {
    private ObservableList<Object> list = FXCollections.observableArrayList();
    private ObservableList<Object> list2 = FXCollections.observableArrayList();
    private ObservableList<Object> list3 = FXCollections.observableArrayList();
    private ObservableList<Object> list4 = FXCollections.observableArrayList();
    private ObservableList<Object> emptyList = FXCollections.observableArrayList();

    @FXML
    private TableView<Object> table;

    @FXML
    private TableColumn<Object, String> tcNazwa;

    @FXML
    private TableColumn<Object, String> tcProducent;

    @FXML
    private TableColumn<Object, String> tcData;

    @FXML
    private TableColumn<Object, String> tcGodzina;

    @FXML
    private TableColumn<Object, String> tcStatus;

    @FXML
    private ComboBox cbTermin;

    @FXML
    private ComboBox cbNazwa;

    @FXML
    private ComboBox cbProducent;

    @FXML
    void initialize() {
        cbTermin.setItems(list2);
        FetchDataFromTableZapisyAdmin fetchDataFromTable2 = new FetchDataFromTableZapisyAdmin();
        fetchDataFromTable2.addData();
        ArrayList listTerminy = new ArrayList<>();
        for (int i = 0; i < fetchDataFromTable2.dataList.size(); i++) {
            listTerminy.add(fetchDataFromTable2.dataList.get(i) + " " + fetchDataFromTable2.godzinaList.get(i));
        }
        list2.addAll(listTerminy);

        Szczepionki szczepionki = new Szczepionki();
        szczepionki.getHashmap();

        cbNazwa.setItems(list3);
        ArrayList listNazwy = new ArrayList<>();
        listNazwy = szczepionki.getKeys();
        list3.addAll(listNazwy);

        cbProducent.setDisable(true);

        cbNazwa.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
            cbProducent.setDisable(false);
            cbProducent.setItems(emptyList);
            cbProducent.setItems(list4);
            ArrayList listProducenci;
            ArrayList emptyList;
            listProducenci = szczepionki.getValuesForKey(String.valueOf(newValue));
            emptyList = szczepionki.getValuesForKey(String.valueOf(oldValue));
            if (emptyList != null) {
                list4.removeAll(emptyList);
            }
            list4.addAll(listProducenci);
        });

        tcNazwa.setCellValueFactory(new PropertyValueFactory<>("nazwaSzczepienia"));
        tcProducent.setCellValueFactory(new PropertyValueFactory<>("producent"));
        tcData.setCellValueFactory(new PropertyValueFactory<>("data"));
        tcGodzina.setCellValueFactory(new PropertyValueFactory<>("godzina"));
        tcStatus.setCellValueFactory(new PropertyValueFactory<>("status"));
        table.setItems(list);
        FetchDataFromTableUzyt fetchDataFromTableUzyt = new FetchDataFromTableUzyt();
        fetchDataFromTableUzyt.addData(StartController.getUzytkownikId());
        list.addAll(WidokUzytkownika.getWidok(fetchDataFromTableUzyt.idList, fetchDataFromTableUzyt.nazwaSzczepieniaList, fetchDataFromTableUzyt.producentList, fetchDataFromTableUzyt.dataList, fetchDataFromTableUzyt.godzinaList, fetchDataFromTableUzyt.statusList));
    }

    public void btnZapiszClicked(ActionEvent event) throws ClassNotFoundException, SQLException {
        if (Objects.equals(cbTermin.getValue(), null) ||
                Objects.equals(cbNazwa.getValue(), null) ||
                Objects.equals(cbProducent.getValue(), null))
            JOptionPane.showMessageDialog(null, "Wypelnij wszystkie pola, aby zapisac sie na szczepienie.");
        else {
            String value = String.valueOf(StartController.getUzytkownikId());
            String terminRaw = String.valueOf(cbTermin.getValue());
            String[] termin = String.valueOf(cbTermin.getValue()).split(" ");
            String nazwaSzczepionki = String.valueOf(cbNazwa.getValue());
            String producent = String.valueOf(cbProducent.getValue());

            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/punkt_szczepien?useUnicode=true&characterEncoding=utf-8&serverTimezone=CET");

            String sqlGetPesel = "SELECT pesel FROM pacjenci where id_login='" + value + "';";
            Statement st = connection.prepareStatement(sqlGetPesel);
            ResultSet rs = st.executeQuery(sqlGetPesel);
            String pesel = "";
            rs.next();
            pesel = rs.getString(1);

            Date terminData = Date.valueOf(termin[0]);

            String query1 = "CALL czyMozeZaszczepic(?,?,?,?)";
            CallableStatement stmt1 = connection.prepareCall(query1);
            stmt1.setString(1, pesel);
            stmt1.setDate(2, terminData);
            stmt1.setString(3, nazwaSzczepionki);
            stmt1.registerOutParameter(4, Types.BOOLEAN);
            stmt1.executeQuery();
            Boolean result = stmt1.getBoolean(4);

            if (!result)
                JOptionPane.showMessageDialog(null, "Nie możesz zapisać sie na dane szczepienie.\nCzas miedzy szczepieniami tego samego typu musi wynosic co najmniej 365 dni,\nnatomiast miedzy szczepieniami innego typu co najmniej 21 dni.");
            else {
                String sql1 = "INSERT INTO szczepienia(nazwa_szczepienia, producent,status) VALUES ('" + nazwaSzczepionki + "', '" + producent + "','oczekujące');";
                Statement statement = connection.createStatement();
                statement.executeUpdate(sql1);

                String sqlGetId = "SELECT MAX(id) FROM szczepienia";
                Statement st2 = connection.prepareStatement(sqlGetId);
                ResultSet rs2 = st2.executeQuery(sqlGetId);
                int idLogin;
                rs2.next();
                idLogin = rs2.getInt(1);

                String sql2 = "INSERT INTO wizyty(data, godzina, pesel_pacjenci, id_szczepienia) VALUES('" + termin[0] + "', '" + termin[1] + "', '" + pesel + "', " + idLogin + ");";
                Statement statement2 = connection.createStatement();
                statement2.executeUpdate(sql2);

                table.getItems().clear();
                table.setItems(list);
                FetchDataFromTableUzyt fetchDataFromTableUzyt = new FetchDataFromTableUzyt();
                fetchDataFromTableUzyt.addData(StartController.getUzytkownikId());
                list.addAll(WidokUzytkownika.getWidok(fetchDataFromTableUzyt.idList, fetchDataFromTableUzyt.nazwaSzczepieniaList, fetchDataFromTableUzyt.producentList, fetchDataFromTableUzyt.dataList, fetchDataFromTableUzyt.godzinaList, fetchDataFromTableUzyt.statusList));
                Statement stDelete = connection.createStatement();
                String deleteRowSql = "DELETE FROM terminy WHERE data='" + termin[0] + "' AND godzina='" + termin[1] + "';";
                stDelete.executeUpdate(deleteRowSql);

                cbTermin.getItems().removeAll(cbTermin.getItems());
                cbTermin.setItems(list2);
                FetchDataFromTableZapisyAdmin fetchDataFromTable2 = new FetchDataFromTableZapisyAdmin();
                fetchDataFromTable2.addData();
                ArrayList listTerminy = new ArrayList<>();
                for (int i = 0; i < fetchDataFromTable2.dataList.size(); i++) {
                    listTerminy.add(fetchDataFromTable2.dataList.get(i) + " " + fetchDataFromTable2.godzinaList.get(i));
                }
                list2.removeAll(terminRaw);
                list2.addAll(listTerminy);

                Szczepionki szczepionki = new Szczepionki();
                szczepionki.getHashmap();

                cbNazwa.getSelectionModel().clearSelection();
                cbNazwa.setItems(list3);

                cbProducent.setDisable(true);

                cbProducent.setItems(emptyList);
                cbNazwa.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
                    cbProducent.setDisable(false);
                    cbProducent.setItems(emptyList);
                    cbProducent.setItems(list4);
                });
                JOptionPane.showMessageDialog(null, "Pomyslnie zapisano na szczepienie.");
            }
        }
    }

    public void btnWypiszClicked(ActionEvent event) {
        try {
            WidokUzytkownika widokUzytkownika = (WidokUzytkownika) table.getSelectionModel().getSelectedItem();
            if (String.valueOf(widokUzytkownika.getStatus()).contains("oczekujące")) {
                Class.forName("com.mysql.cj.jdbc.Driver");
                Connection connection;
                connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/punkt_szczepien?useUnicode=true&characterEncoding=utf-8&serverTimezone=CET");

                String sql2 = ("DELETE FROM wizyty WHERE data = '")
                        .concat(widokUzytkownika.getData() + "' AND godzina = '")
                        .concat(widokUzytkownika.getGodzina() + "';");
                Statement st2 = connection.createStatement();
                st2.executeUpdate(sql2);

                String sql3 = ("DELETE FROM szczepienia WHERE nazwa_szczepienia = '")
                        .concat(widokUzytkownika.getNazwaSzczepienia() + "' AND producent = '")
                        .concat(widokUzytkownika.getProducent() + "';");
                Statement st3 = connection.createStatement();
                st3.executeUpdate(sql3);

                String sqlInsert = "INSERT INTO terminy(data,godzina) VALUES('" + widokUzytkownika.getData() + "', '" + widokUzytkownika.getGodzina() + "');";
                Statement stInsert = connection.createStatement();
                stInsert.executeUpdate(sqlInsert);
                table.getItems().removeAll(table.getSelectionModel().getSelectedItem());
                JOptionPane.showMessageDialog(null, "Pomyslnie wypisano ze szczepienia.");
            } else {
                JOptionPane.showMessageDialog(null, "Wybrano szczepienie ktorego status jest inny niz \"oczekujace\".");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Nie wybrano pozycji do usuniecia. Zaznacz odpowiedni wiersz w tabeli.");
        }
    }
}